"GameMenu"
{
  "1"
  {
    "label" "Record pov_demo..."
    "command" "engine recorddemo; beep"
    "OnlyInGame" "1"
  }
  "4"
  {
    "label" "Stop Record"
    "command" "engine stop; zeep"
    "OnlyInGame" "1"
  }
 "5"
 {
    "label" ""
    "command" ""
    "OnlyInGame" "1"
 }
 "11"
 {
    "label" "Mute Noobs"
    "command" "OpenPlayerListDialog"
    "OnlyInGame" "1"
 }
 "12"
 {
    "label" "Status"
    "command" "engine showconsole; clear; status; jpeg_quality 100"
    "OnlyInGame" "1"
 }
 "13"
 {
    "label" "Snd_restart"
    "command" "engine snd_restart; boop"
    "OnlyInGame" "1"
 }
 "14"
 {
    "label" "Download Filter"
    "command" "engine showconsole; toggle cl_downloadfilter none all nosounds"
    "OnlyInGame" "0"
 }
 "15"
 {
    "label" ""
    "command" ""
  }
  "16"
  {
    "label" "Resume Game"
    "command" "ResumeGame"
    "OnlyInGame" "1"
  }
  "17"
  {
    "label" "Re-Connect"
    "command" "engine retry"
    "OnlyInGame" "1"
  }
  "18"
  {
    "label" "Disconnect"
    "command" "Disconnect"
    "OnlyInGame" "1"
  }
 "19"
 {
    "label" ""
    "command" ""
    "OnlyInGame" "1"
  }
"20"
  {
    "label" "Find Servers"
    "command" "OpenServerBrowser"
  }
  "21"
  {
    "label" "Create Server"
    "command" "OpenCreateMultiplayerGameDialog"
  }
  "22"
  {
    "label" "Options"
    "command" "OpenOptionsDialog"
  }
  "23"
  {
    "label" "Watch Demo"
    "command" "engine demoui"
  }
  "24"
  {
    "label" "Real Life"
    "command" "engine quit"
  }
}